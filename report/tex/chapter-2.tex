%You should survey and critically evaluate other work which you have read or otherwise 
%considered in the general area of the project topic. The aim here is to place your 
%project work in the context of the related work.
\chapter{Related Work}

\section{Cryptography Basics}
In order to understand how encryption schemes work we must first learn some properties of the components that
comprise them.
\subsection{XOR($\oplus$)}
Exclusive or(XOR) is a fundamental operation in cryptography. It returns true "if one and not the other" -
\begin{itemize}
    \item[]{$0 \oplus 0 = 0$}
    \item[]{$0 \oplus 1 = 1$}
    \item[]{$1 \oplus 0 = 1$}
    \item[]{$1 \oplus 1 = 0$}
\end{itemize}

Of course this is only relevant to single bits - fortunately bitwise XOR works exactly as we would expect
as there is no overflow to deal with. For example, $11 \oplus 01 = 10$.\\

XOR is the only operation used in the one time pad - a perfectly secure encryption scheme if correctly 
implemented. For a one time pad we have a "pad" filled with random bits of the same size as the data
we would like to encrypt. We take the XOR of the pad and the data we would like to encrypt and assuming 
that the pad is known only to authorised people and that the bits on the pad are truly random no information
can be learnt from the ciphertext without the pad. \\

If we have a pad $P$ and document $D$ we can show that this encrypts and decrypts correctly because
$P \oplus D$ is some ciphertext that can only be generated by both the document and the pad. To decrypt we
simply use $P \oplus D \oplus P$. Since the order of XOR is irrelevant we can switch $D$ with a $P$ and we know
that anything XOR itself is 0, thus $P \oplus D \oplus P = D$. \\

The reason a one time pad can only maintain the perfect secrecy attribute is because an attacker with both ciphertext
documents can find a lot of data from the XOR of these. Since the ciphertext is $P \oplus D$ the XOR of two ciphertext
documents cancels out the $P$ and gives $C_{1} \oplus C_{2}$ and allows some data to be leaked.

\subsection{Block Cipher}
\begin{figure}[h!]
    \caption{Bijection diagram}
    \centering
    \includegraphics[width=0.5\textwidth]{permutation.jpeg}
\end{figure}
A block cipher is a symmetric key function that uses a deterministic encryption algorithm based on a key. It operates on blocks
of a fixed size and maps each block to another block. Since a block cipher is a bijection every block is mapped once to another block
and can map to itself. \\

Since it is a deterministic function, a word encrypted with a key will always return the same ciphertext regardless. If for example,
the word "test" produced the ciphertext "eage" every occurrence of the word "test" would be mapped to "eage" in a document.

\subsection{Stream Cipher}
We contrast a block cipher with a stream cipher. A stream cipher encrypts a sequence of bits of varying length. It is a non-deterministic 
cipher that uses pseudorandom numbers to encrypt. A key characteristic is that the same characters at different positions in the document 
will be encrytped with a different character.  For example, the character 'A' used at the start of the document will be encrypted differently 
from the same character at the end of the document or even one character after the first. \\

A stream cipher uses a counter in the generation of pseudorandom numbers - with this the same values can be encrypted with a different resulting
ciphertext as the counter will be at a different value. Instead of starting the counter at zero, the stream cipher takes an initialisation vector(IV)
to start with. The IV should be unique but does not need to be kept private and is usually prepended to ciphertext.

\subsection{Hash Function}
A hash function is a function that maps a block of text of any length to a fixed-length block of text. It can be used to compare files by computing
their hash values and comparing these. This is obviously preferable to going line by line and checking that the values match. \\

A hash function is irreversible and is not a form of encryption. It is however used as part of cryptographic schemes such as the one we will use.

\section{Deterministic Encrypted Search}
\begin{figure}[h!]
    \caption{Deterministic Encrypted Search}
    \centering
    \includegraphics[width=0.5\textwidth]{deterministic.jpeg}
\end{figure}
This method\cite{deterministic} of encrypted search is a fairly naiive implementation as we will see.
For this method the data is encrypted using a block cipher $F$ so that there is no randomness and 
$F(W)$ will always map to ciphertext $Y$ where $W$ is some word. \\

With this we can encrypt our data on the client side and upload the ciphertext to our untrusted server.
When we want to search on the data we just send the server $F(X)$ and have the server search for $F(X)$ 
in the same manner as a plain text search. \\

This is a \textbf{very} basic scheme for searching encrypted data and has multiple shortcomings.
The biggest shortcoming is that since it uses a block cipher the untrusted server can learn about the data
even though it is encrypted by using frequency analysis. Over time the server can use our searches and the frequency
of characters in the ciphertext to make guesses at what they are mapped to. \\

A slight variation and debatably an improvement to this is to pre-compute an index for the documents and encrypt
the documents using a stream cipher and the index using our block cipher. Then when we want to search the server
can search the index in the same manner as before and return the list of files that contain our search term without 
learning anything about the documents themselves. \\

Ultimately this suffers from the same problem previously described but can perform faster only including the time taken
to search and not the time taken to generate the index. \\

Since the server is untrusted and we would prefer it didn't know anything about our data this is more than enough 
of a reason to not use this scheme. 

\section{Crococrypt}
Crococrypt\cite{crococrypt} is a tool that allows users to mirror a folder to an encrypted folder on some cloud storage service.
With this, the user has a plaintext copy of their files on their end but the server side holds an encrypted version - much like our 
application. \\

While Crococrypt does not allow encrypted search it is very similar to the style of application 
this project aimed to produce. Our directory mirroring future work feature was inspired by this application.

\section{CryptDB}
CryptDB\cite{cryptdb} is a system similar to ours that allows users to search SQL queries on encrypted data. The idea is again
similar to ours in that the aim is to prevent database administrators from being able to read the contents of users 
data without permission. \\

It works by making use of SQL aware encryption schemes. Incidentally it makes use of the scheme described in Song et als paper to perform
queries for words on an encrypted database.
