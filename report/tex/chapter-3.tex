%Describe in detail, with examples if appropriate, the problem which you are trying 
%to solve. You should clearly and concisely specify the problem and should say how 
%the specification was arrived at. You should also provide a general discussion of 
%your approach to solving the project problem.
\chapter{Problem Description and Specification}
Encryption schemes tend to be aimed at making sure that data is encrypted and not 
whether or not it is usable in the encrypted form. This project seeks to look into 
a method for searching on encrypted data. \\

Without any thought we store our documents and personal data on servers that we have no
control over and no real guarantee that our data will be kept private. More than ever this 
is a growing problem. If we were able to upload our documents and data to these services in 
an encrypted form(useless to the service provider or anyone looking at it) but still be able
to search it and find the documents we want it would give piece of mind to the user instead 
of having to blindly trust the service provider. \\

It is also common for users to be working on low bandwidth such as from a mobile device. It
would be helpful in this situation to be able to narrow down the documents that we need to 
download from the server. For example, the user could search for all documents with the word
"URGENT" and download only these instead of the entire document set. \\

Put simply, the problem this project sets out to solve is to allow a user to upload a set of 
documents to our "untrusted server" and be able to search them without 
our server knowing anything about their documents. After receiving a list of matches the user
can download the files selectively.\\

Since the trend in recent times is to host such services on cloud platforms we will also
investigate how well our server works on Amazon Web Services and leverage some of their 
products to extend our application.\\

It is desirable to allow the user to search using regular expressions as it allows a degree of
flexibility in the terms that they search for. We can implement this in a limited form. \\

The main components of the produced application are described below.
\section{Problem Components}

\subsection{Encrypted Search}
Clearly the main issue to be addressed by this project is encrypting data in such a way that 
it can be searched by the user. This is be accomplished by implementing the scheme described in 
Song et al's paper as will be explained below.

\subsubsection{Practical Techniques for Searches on Encrypted Data}
The paper\cite{sed} that this project is based on is written by Dawn Xiaodong Song,David Wagner and Adrian Perrig. The paper begins by outlining a basic
scheme with some of the features we desire in a searchable encryption scheme and goes on to adjust the scheme to meet our needs. \\

The final scheme is described by a diagram that shows the stages to encrypt the data. The paper provides proofs of the security of the scheme and that it achieves
the goals that it sets out to achieve. \\

To implement the scheme we require a few cryptographic primitives as detailed below:  

\begin{itemize}
    \item{Pseudorandom generator $G$ - a stream cipher}
    \item{Pseudorandom function $F$ - a hash function}
    \item{Pseudorandom permutation $E$ - a block cipher}
\end{itemize}

For each of these we have an accompanying key - we will call these $k^{\prime} , k^{\prime\prime} and k^{\prime\prime\prime}$ for the pseudorandom generator, function and permutation 
respectively. When we say $E_{k^{\prime\prime\prime}}(W_{i})$ we mean to call $E$ on $W_{i}$ using $k^{\prime\prime\prime}$ as the key.\\

We also select a boundary to split the word at - we'll call it $x$. We split our padded word on this boundary in encryption, decryption and searching - 
the scheme will not work if we use a different value for $x$ than what was used in the encryption of a document. As we can see in the diagram we hash $S_{i}$ and truncate it 
to $x$ if required.  Note that by truncating the value we make it more and more likely to have overlapping hash values. For example: \\
\begin{lstlisting}[language=Python,frame=lines,basicstyle=\small, numbers=left]
boundary = 11

a = hash('test')
>>> lkasdjklflkjdf
b = hash('some other thing')
>>> eirowerpoewjdf
a[boundary:] == b[boundary:]
>>> True
\end{lstlisting}
While these are made up hash values we can see that if we truncate the hash value we leave a small number of possible values and make overlapping likely.
As a result of this overlap the scheme can return false positives.
\\

\begin{figure}[h!]
    \caption{The encryption scheme provided by Song et Al}
    \centering
    \includegraphics[width=0.5\textwidth]{encryption.jpg}
\end{figure}

\paragraph{Encryption}

To encrypt some plaintext document we do the following:
\begin{enumerate}
    \item{Tokenize the input - whitespace and punctuation are tokens}
    \item{Do the following to each token $W_{i}$}
        \begin{enumerate}
            \item{Encrypt the token using the pseudorandom permutation $E_{k^{\prime\prime\prime}}$ to give $E_{k^{\prime\prime\prime}}(W_{i})$}
            \item{Split $E_{k^{\prime\prime\prime}}(W_{i})$ at the boundary $x$ to give $L_{i}$ and $R_{i}$ for the left and right sides of the boundary respectively}
            \item{Generate $S_{i}$ by taking $x$ bytes from the pseudorandom generator $G_{k^{\prime}}$}
            \item{Generate a word-specific key $k$ as $F_{k^{\prime\prime}}(L_{i})$}
            \item{Generate our position specific word key as $F_{k}(S_{i})$}
            \item{Concatenate $S_{i}$ with $F_{k}(S_{i})$ to give our key to encrypt}
            \item{Finally we XOR the key we just created with $E_{k^{\prime\prime\prime}}(W_{i})$ to give the ciphertext $C_{i}$}
            \item{Append $C_{i}$ to our output ciphertext}
        \end{enumerate}
\end{enumerate}
Essentially we are generating a key for every token based on its position in the document and its contents. After encrypting we can send the
ciphertext to the untrusted server. We will see below how this allows us to search on our data.

\paragraph{Search}
Before the server can search our encrypted documents we must prepare our search term on the client side. To prepare a search term we do the following:\\

\begin{enumerate}
    \item{Encrypt the search term using the pseudorandom permutation $E_{k^{\prime\prime\prime}}$ to give $E_{k^{\prime\prime\prime}}(W_{i})$}
    \item{Split $E_{k^{\prime\prime\prime}}(W_{i})$ at $x$ to give $L_{i}$ and $R_{i}$ respectively}
    \item{Generate the word specific key $k$ as $F_{k^{\prime\prime}}(L_{i})$}
    \item{Send $E_{k^{\prime\prime\prime}}(W_{i})$ and $k$ to the server}
\end{enumerate}

Given $E_{k^{\prime\prime\prime}}(W_{i})$ and $k$ the server can search an encrypted document by doing the following: \\

\begin{enumerate}
    \item{Read a block $C_{i}$ from the input until there are none left and do the following}
        \begin{enumerate}
            \item{XOR $E_{k^{\prime\prime\prime}}(W_{i})$ with $C_{i}$ to give us the key used to encrypt the block}
            \item{Split the key at $x$ to give us $S_{i}$ and $F_{k^{\prime\prime}}(S_{i})$ from the left and right respectively}
            \item{Generate a position specific word key as $F_{k}(S_{i})$ and truncate it to the same size as $F_{k^{\prime\prime}}(S_{i})$}
            \item{If the position specific word key we just generated is the same as $F_{k^{\prime\prime}}(S_{i})$ then we have a potential match}
            \item{If they are different then keep looking}
        \end{enumerate}
\end{enumerate}

If we are given an incorrect $k$ or $E_{k^{\prime\prime\prime}}(W_{i})$ then our key generation stages will give a different result and the server learns nothing.
Also, since the term is pre-encrypted the server doesn't know what we are looking for - even if it XORs the encrypted term the encryption key returned will be 
different. \\

Our search process seeks to reproduce the position specific word key with the information provided. As was previously discussed there can be some overlap if the 
right side of the key is short.

\paragraph{Decryption}
When we find a match we would like to decrypt the encrytped file on the client side. Of course on the client side we have our three keys. \\

To decrypt a ciphertext document we do the following:
\begin{enumerate}
    \item{Read blocks $C_{i}$ from the input until there are none and do the following on each}
        \begin{enumerate}
            \item{Take $x$ bytes from the pseudorandom generator $G_{k^{\prime}}$ to give $S_{i}$}
            \item{Split $C_{i}$ at $x$ and XOR the left side with $S_{i}$ to give us $L_{i}$ from the encryption process}
            \item{Generate the word-specific key $k$ as $F_{k^{\prime\prime}}(L_{i})$}
            \item{Generate the position specific word key as $F_{k}(S_{i})$}
            \item{Concatenate $S_{i}$ with $F_{k}(S_{i})$ to give the key used to encrypt $C_{i}$}
            \item{XOR the key used to encrypt $C_{i}$ with $C_{i}$ to give $E_{k^{\prime\prime\prime}}(W_{i})$} 
            \item{Finally we decrypt $E_{k^{\prime\prime\prime}}(W_{i})$ to give $W_{i}$}
            \item{Add $W_{i}$ to our output}
        \end{enumerate}
\end{enumerate}

\subsection{Regular Expression Support}
This is a relatively simple addition and is outlined in Song et al's paper. 
It relies on expanding an expression into a list of queries and running all of the queries. 
In other words, a query with [a-z] would generate 26 different queries to be run on each file. This quickly becomes a large task, hence we only
support a very limited subset of regular expressions.

\subsection{File Server}
The user should be able to upload their pre-encrypted files to our server and be able to
search them. It should also include basic interactions such as adding files and downloading files.

\subsection{Cloud}
As with any other form of parallelisation there will be some work involved in altering the 
application to work on a cloud platform effectively. This involves identifying areas where 
the search process can be sped up by paralellising and perhaps moving the file storage to an
external file store that allows flexibility in terms of scaling. \\

Ideally this would be transparent to the user and act as though it were just a typical file 
server.

\subsection{Client Application}
For the sake of simplicity our original client is a text based client that allows us to interact
with our encrypted file server.

\subsection{GUI Client}
Since a text based client leaves much to be desired for in terms of usability for non-technical 
users we develop a basic client application with a GUI that provides access to our operations provided
by our encrypted file server.

\section{Objectives}

The following are the core objectives that must be accomplished in order to ensure that the 
application meets the goals outlined above. They remain unchanged from the original specification.
\subsection{Core}
\begin{itemize}
    \item{Create file server}
    \item{Add functionality to search encrypted data}
\end{itemize}

\subsection{Extended}

The following are extended goals that would be nice to have and will be added if time permits.
\begin{itemize}
    \item{Migrate application to a cloud service}
    \item{Support regular expressions in a limited form.}
    \item{Add a GUI client}
\end{itemize}

\section{Problem Approach}

The approach to this project has been largely dictated by my lack of any real exposure to 
cryptography prior to starting this. This has made a huge difference to the schedule of the project
and the subsequent changes to the schedule. \\

The path taken in development was to first develop our library of functions to carry out our encryption
tasks first. We accomplished this by creating the small functions to carry out the stages of our encryption process. 
With this library we are able to investigate hosting the application on AWS and create client applications to use 
the library.\\

The result is a small library of functions that are used to encrypt, decrypt and search data - this is 
used on the client and server side since there is a large amount of common functions to each operation. We also
have a text based client and a graphical user interface.
