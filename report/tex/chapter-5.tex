\chapter{Detailed Design and Implementation}

\section{Implementation Languages}

\subsection{Python}
While there are a large number of choices in languages to write an application such as this, Python\cite{python} was chosen partly due 
to the author's familiarity with it and partly due to the readability it provides. \\

Python has an extensive standard library that covers most of our requirements and an extremely large catalog of third party libraries.
This means that we are able to focus on writing our application with minimal effort in terms of having to write extra tools that we 
may need.\\

With Python it is easy to go from pseudocode to working code and it becomes a simple task to debug any problems as the code is 
easy to read and understand/compare with the pseudocode. By making use of multiple, small functions the code produced is simple 
to debug and trace the cause of any problems. \\

The obvious alterntive would be Java as is taught on this course and this would likely have brought some performance benefits.
we would argue that the readability provided by Python outweighs this benefit, at the very least in demonstrating how the application
works and that the problem doesn't lend itself very well to an object oriented design.

\subsection{Golang}
As the Python implementation was relatively slow, a version of the search function was written in golang to offer 
a comparison to confirm that the scheme is not slow and that the reason for speed issues was on the Python end. \\

Golang\cite{golang} is a language written by Google and provides an extensive standard library that includes everything we
need to implement our scheme. Ultimately, we were unable to use this version as there is no EMR support for 
golang and AWS libraries for the language are relatively young. \\

While AWS allows binaries to be used, it is a challenge to debug the application and thus, we use the Python 
version as the final version.

\section{Third Party Tools}
We make use of external libraries where our standard library provided by Python does not provide certain
functionality or we would like to make use of external APIs.

\subsection{pytest}
pytest\cite{pytest} is a testing library that allows us to write unit tests without the boilerplate code usually associated with this.
After importing the pytest module functions with "test" in the function name will be treated as test cases. It also provides
support for test cases in docstrings which we make slight use of. For example, any docstrings with what looks like Python console 
commands will be evaluated as a test case. For example a docstring with  

\begin{lstlisting}
>>> 4 + 4
>>> 8
\end{lstlisting}

Will be run as though we had a test case asserting that 4 + 4 == 8. This gives more reason to write more descriptive documentation
that better outlines the expected behaviour of a function. 

\subsection{PyCrypto}
The Python standard library leaves much to be desired for with regards to the cryptography package. It includes 
a few hash functions that don't remotely meet our requirements. Fortunately, they link to the PyCrypto\cite{pycrypto} library
which does meet our requirements. \\

PyCrypto provides an extensive API to a variety of cryptography operations and ciphers, including the block 
cipher, stream cipher, hash functions and XOR that we need to implement our scheme.

\subsection{Boto}
Boto\cite{boto} works as an API wrapper for AWS and allows us to interact with AWS services using high level function 
calls. It allows us to interact with our S3 buckets and to provision clusters/send work to them with minimal effort. \\

With this we can define the size of the instances used in our cluster, the number of worker nodes to be used and other
parameters that we may need to change.

\subsection{Tkinter}
Tkinter\cite{tkinter} is a commonly used library used to create graphical user interfaces. It makes use of several Python features 
such as higher order functions to make common tasks such as click handlers very simple. For example, in Java to create 
a click handler we would need a class to handle the clicks and determine what to do. In Tkinter we call the "bind" function
on the UI element we'd like to bind to, specify a key to bind to(in this case a mouse key) and give a function to call on 
click. \\

\section{Development Tools}

\subsection{Linode VPS}
The application was initially developed and hosted on a Linode VPS. This allowed the application to be tested
by searching on files remotely and not locally in some other directory. It also allowed a degree of flexibility 
with regards to the time an application can be run for through the use of tmux. \\ 

This is vital for long running processes such as the scraper used to obtain our dataset. With this we are able 
to run the scraper, detach and exit the ssh connection without worrying about the script stopping. This also 
applies to encrypting, decrypting and searching a large dataset without losing productivity due to high CPU or
memory usage.

\subsection{Vim}
Since we developed the application on a remote, headless server we use a text editor that supports this. 
Obviously text editor choice is completely based on the preference of the user but it is worth mentioning just
how useful it was to use an editor that provides easy access to the terminal to run our application and even 
helps with testing. In our case we were able to test the decrypt function easily by making use of vimdiff to 
check the original plaintext against the decrypted file to ensure that we have the correct result.

\subsection{AWS Developer Console}
\begin{figure}[h!]
    \caption{AWS Developer Console}
    \centering
    \includegraphics[width=0.9\textwidth]{devconsole.png}
\end{figure}

The AWS developer console is essential to debug an application on AWS as most outputs are sent to S3.(they even
send bills to S3) While it would be possible to download the files using the AWS command line client it does not
provide the ability to explore directories and effectively find the problems - especially if the user is 
unfamiliar with the directory structures used. \\

From the developer console we are able to start a cluster and send jobs to it, in our case we send searches to 
it. When the job ends either by failing our completing successfully the output logs are sent to an S3 folder 
including stdout, stdin and syslog. These are our only way of seeing what goes on in the execution of our code
as there is no method of introspection to view the program as it runs. \\

It also allows us to inspect our S3 buckets and the files contained in them. This made it easy for us to verify that
our files were being correctly uploaded and to investigate log files as required.
\subsection{Bitbucket}
As with any project of considerable size, version control is required. For this we use Bitbucket - the main 
reason for Bitbucket over Github is that it has infinite private repositories that were useful in previous 
years. \\

By making use of the issue tracker provided we were able to create a consolidated list of bugs and possible 
improvements to our code instead of having comments with "TODO: something" dotted around the code base. \\

Of course, most importantly it allowed us to roll back any changes when required and provided a degree of safety
knowing that the codebase is hosted elsewhere and not just locally.

\section{Features}
Here we will discuss the implementation of our application features and explain the thought process behind 
decisions our implementation. \\

For those unfamiliar with Python syntax, function names beginning with \_ are functions private to the
module. This avoids polluting the global namespace by importing the module and including unwanted functions
that neither the client or server should need to use other than implicitly through the functions left visible. \\

Since our primary encryption operations are to encrypt a file, decrypt a file, prepare a search term and to
search a file only these functions have been left public.

\subsection{encrypty.py}
\subsubsection{\_tokenize\_file}
This is a small function that tokenizes a file, including punctuation and whitespace as tokens using 
regular expressions. It returns a list of tokens to be used at a later stage.

\subsubsection{\_pad\_word}
Since a block cipher can only operate on blocks of fixed length and clearly not all tokens will be this size 
we are forced to pad tokens to our block size of 16. As we include whitespace in our tokenizer we opt to pad
with \textbackslash x14 - a non-printing ASCII character. This means we can easily identify padding since a 
non-printing character shouldn't appear in a text file.

\subsubsection{\_encrypt\_word/\_decrypt\_word}
We pre-encrypt our tokens using a block cipher - in this case AES in CBC mode with a constant IV. These functions
take a token and return the encrypted/decrypted version of that word using some key.

\subsubsection{\_generate\_bitstream}
As part of our scheme we must generate a pseudorandom bitstream using a stream cipher. For this we use AES in
CFB mode. Pycrypto does not allow the stream used to encrypt to be seen - to get around this we pass a string
of \textbackslash x00 to the function of the length of the file * our block size to accomodate for our padding.
This provides us with the pseudorandom stream used to encrypt our text since intuitively, it works by computing
the XOR of the stream and input. As was shown earlier, anything XOR 0 is itself and thus we have the stream CFB
used to encrypt.

\subsubsection{\_xor\_words}
Most languages provide an XOR operator for integers and such, but in our case we would like to compute the XOR
of strings using the ASCII values at each index. PyCrypto provides an "XORCipher" that allows us to compute the 
XOR of two strings using the same interface as with our other ciphers.

\subsubsection{\_hash\_stream}
As part of our scheme we need to combine a string with a key and hash it. In our case we choose to concatenate 
the string and key and then hash the result using SHA1.

\subsection{Encryption}
Encryption of a file is a fairly expensive process as there is a fair amount involved as we will see.
When we encrypt a file we operate on 16 byte blocks - tokens over 16 bytes are truncated to 16 bytes.\\

First off we tokenize the file input using our \_tokenize\_file function and encrypt the 
tokens using our \_encrypt\_word function. This gives us a list of pre-encrypted tokens 
the same as $E(W)$ from the earlier diagram.\\

Then we generate a random IV using pycrypto's secure random function. Keep in mind that the 
IV must only be unique but does not need to be secure as it is not a key. We then generate 
a stream of pseudorandom bits
using the IV we generated and a key. We generate a stream large enough for the entire file to save us the 
overhead of generating more for each token as this would quickly add up to a large amount of time. \\

In a similar vein we store the final encrypted form of each token in a list instead of writing them as they
are encrypted. \\

Now we loop on every CBC encrypted token in our list of tokens and perform our encryption. First we take the
first x bits from our bitstream where x is the boundary we have chosen to split our words on and remove these 
bits from the stream to give us $S_{i}$ from the earlier diagam. Then we hash the first 
x bits of our CBC encrypted token with a key to generate a word specific key(k). \\

Next we hash the first x bits we stored earlier from our stream and the word specific key we just generated 
and truncate this to 16 bits minus x($F_k (S_i)$). Finally, we XOR the CBC encrypted token and the bits 
taken from the bitstream plus the truncated value we just generated and append this to our list. \\

With this we have a list of encrypted strings that could be used in some other application but in our case
we open a file and write the IV to the front and then the ciphertext, appending ".tmp" to the name since it 
will be uploaded to our server and deleted locally. \\

In the paper the process describes splitting the tokens - instead we make use of Python's 
slice notation to take the left and right side as and when we need them. In hindsight, perhaps
there could be a performance benefit to storing them in a variable instead of performing 
this split when we need them.

\subsection{Decryption}
As we would expect, decryption is implemented as an opposite of encryption.\\

First we read the first 16 bytes of the file to retrieve the IV that we used in the encryption process and 
then generate the bitstream from the stream cipher using our recovered IV and the key used to encrypt. \\

As with encryption we store our tokens in a list to avoid writing to file for every single token.  Since our 
scheme encrypts using 16-byte blocks we read in 16-bytes from our file and do the following. \\

We take the first x(the same x used in encryption) bits($S_i$) from our bitstream and move along the stream. 
We then XOR the first x bits of the current block with the bits we just generated to retrieve the left hand side
of the CBC encrypted word($L_i$). \\

We hash this with the key we used in encryption to generate our word specific key and then XOR the current block
with the hash of the bits from the bitstream and the word specific key to give us the CBC encrypted token. \\

Finally we decrypt our CBC encrypted token and remove \textbackslash x14 characters from it to give the 
plaintext which is appended to our list and written to file.
\subsection{Search}
Our final basic operation is to allow the user to search the data we have encrypted. We do this by sending a
pre-encrypted search term and word specific key.

\subsubsection{Preparing A Search Term}
\begin{lstlisting}[frame=lines,basicstyle=\small, numbers=left]
prep_search
    initialize an output list
    for every term in expand_regex(query)
        c = encrypt the term using our block cipher
        k = combine the left side of term with the key and hash them 
        add the tuple (c,k) to the output list

    return output list
\end{lstlisting}

Preparing our search term is a simple process - we take a list of queries and prepare each of them
by encrypting the search term using CBC encryption and then we hash the left x bytes of our encrypted 
term with the key provided to generate the word specific key. We return a list of tuples containing 
the pre-encrypted word and word specific keys. \\

In Song et Al's paper the search term is sent by concatentating the pre-encrypted word and word specific key although
in our case we use a tuple. We make use of Python's serialization package pickle to serialize the list and push it to 
S3 using the current timestamp as the key. Later, we tell EMR to get this list of tuples for our search.\\

\subsubsection{Performing The Search}
As will be explained later, moving to EMR required a few changes to our search function and as a result we 
read files from stdin and our output is sent to stdout. We are also unable to break when we find a match, 
to get around this we use a boolean flag to keep a track of whether we have found a match. If we have, we will
keep consuming input and doing nothing with it until there is none left. \\

Here we describe the generic method for searching - our version has some small changes to support regular expressions
and to run on EMR. \\

Our search function takes the query created using prep\_search and splits it at 16 bytes on the left. The left
side is the search term and the right side is the word specific key. \\ 

We read a block from stdin and do the following for every search term in our list of terms. XOR the block with the search 
term to give the key used to encrypt the word. Then we take the left x bytes of this key($S_i$) and hash it with the word 
specific key taken from the right side of the query string passed to the function. \\

If this is the same as the bits after 16-x then we have a potential match and print the file we are 
reading and set our found flag.

Note that since we don't reveal the key used by our block cipher to the server that the server can't learn
anything about our plain text.
\subsection{AWS Integration}
The original prototype of the file server was written as a client application that connected to a TCP server
acting as our file server. In order to make use of AWS features some changes had to be made as we will see.
\subsubsection{S3}
The changes required to move file storage to Amazon S3 were very minimal since the library used provides 
a very high level interface to S3. The prototype supported adding files, deleting them, searching them and 
downloading/decrypting them. \\

As a result of the client application being very modular in terms of the operations supported it was easy
to edit the functions without causing any knock-on effects with the rest of the code. For example, as expected,
the function to add files encrypts the filename given and pushes it to S3 instead of connecting to the server 
via sockets and transferring the file. \\

Likewise with getting files, it works as expected. The S3 API allows files to be retrieved easily
by name since at its core, S3 is essentially a key-value store. We can download files and decrypt them
to be usable by the user.

\subsubsection{EMR}
Moving to EMR required a few code changes and a lot of troubleshooting to make it work. When we provision a 
cluster we are able to give it a "bootstrap" script. This script is run on every node in the cluster and allows
us to install the required Python libraries. \\

Our bootstrap script is a four or five line bash script that installs a package manager to install Python libraries
and installs the libraries that our application needs. \\

To run a job on the cluster we specify a mapper script - a script that is run on every member of our input. We
provide a reducer that is run on the outputs of the mapper to perform some kind of aggregation or in our case
to piece them together into one file. We also provide a dataset in the form of an S3 directory and an output 
directory. \\

To further elaborate on the mapper and reducer we keep in mind that EMR splits our input data and divides it 
between the available resources, resulting in multiple files with our output. With our search function the 
result is multiple output files including pontential matches from our search. Our reducer function is then run 
on each of these output chunks and performs some operation with it - in Amazon's example they compute a word
count using the reducer. \\

At the lower level, Hadoop does something similar to the following:
\begin{lstlisting}[language=bash]
cat file1 file2 ... filen | ./mapper | sort | ./reducer
\end{lstlisting}
For those unfamiliar with Bash syntax the command above prints the contents of each of the files passed in and
passes that to the mapper script, sorts the output of the mapper and passes this to the reducer. \\

A key point to note in the move from a local filesystem to EMR is that we do not read from files directly and 
instead we read them from stdin - intuitively, since we read from stdin in our mapper and reducer scripts our 
output is read from stdout.(in other words printed out to the console) \\ 

Being unable to directly access a file caused some confusion since the output of our script should be a list 
of names of files that may contain a match. Fortunately, EMR sets an environment variable 
"mapreduce\_map\_input\_file" containing the name of the file currently being worked on. \\

If we break out of our mapper before we consume the input file EMR throws a broken pipe error. The original
worry was that this would cause our search to run in o(n) time every time since we would have to do something
with the entire file instead of returning immediately after finding a match. We solve this by including a 
boolean flag to track whether a match has been found and to consume the input without doing anything if it has.
This way we still need to read the entire file but don't incur as large of a performance decrease as would be 
the case if we continued searching.\\

When we run a search we write the pre-encrypted search terms to an S3 bucket using the current timestamp as 
a key. We call the mapper script and pass the timestamp in and the search function retrieves the serialized string
from S3 and de-serializes it to be used by the search function. \\

When a node in our cluster finishes processing its allocated data it returns its respective chunk of output to
a file - in other words we have an output file per node. Since we have no aggregation tasks or the like we use
our reducer to simply join the chunks together into one output file.
\subsection{Regex Support}
As is described in Song et Al's paper, regular expressions can be supported fairly easily by passing multiple
queries to the server. In other words, the query [ab]a would generate two queries [aa,ba]. This quickly becomes
a problem as we begin to use wildcards and large character classes so we choose to include only a few regular
expression features to show that it is achievable. \\

We implement an expander for regular expressions using a recursive function that evaluates regular expressions
from left to right and adds strings with no more regular expression symbols to the set of finished queries. This
approach allows us to support multiple regular expression symbols in one string with relative ease instead of 
having to actively track which symbols have been expanded on the input string. \\

We make extensive use of slices to cut the expression we are currently working on to remove optional parts and 
the regular expression part({x}/[ab]) to replace these with the desired text. Using the same approach we are 
able to extract the text between opening and closing braces to give us the specific parts that we need. \\

Our supported expressions are optional characters using $?$ - for example, $a?b = ['ab','b']$. We also support
character classes in the form $[ab] = ['a','b']$ and repeating characters with the form $a{3}b = ['ab','aab','aaab']$. \\

Unfortunately we have no validation for regular expressions and the application will hang if given an 
incorrect expression.

\subsection{client\_functions.py}
Client functions is a module we use to reuse any functionlity that is common to client applications that use 
our encrypted file server. It provides a few functions to interact with S3 and EMR.

\subsubsection{Search}
We provide a function that allocates a search job to an EMR cluster. We prepare a search and serialize then add it
to an S3 bucket using the current timestamp as the key to be used. Then we use the boto library to create an EMR job and run it.
If we are passed a cluster ID then we run the job on that cluster - otherwise we provision one and use that. The function call
to run a job on EMR does not block and so we implement a very basic block that checks the cluster periodically to check if it has 
completed its job. \\

When the job is complete we take the outputs from an S3 directory also named using the timestamp and return them to the client with
the cluster ID we used - that way the client can use the cluster again.

\subsubsection{Add}
We also have a function to allow users to add files to the file server. We make use of the encrypty packages encrypt\_file function
to encrypt a file using the keys supplied. We then use the S3 API wrapper provided by boto to add the encrypted file to S3.

\subsubsection{Get}
Our final interaction with our file server is to get files from it. We use boto to download our encrypted file from S3 and use the 
decrypt\_file function from our encrypty module to decrypt the file for the user to use.

\subsubsection{Valid\_key}
We provide a small template function to validate a key - we currently only check the size of the key to make sure it is the same as 
our block size although we could extend this to provide a more sophisticated validation method.
\subsection{Text Client}
\begin{lstlisting}[language=Python,frame=lines,basicstyle=\small, numbers=left]
operators = ['add','search', 'get']

parser = argparse.ArgumentParser(description='Add, get or search files on an 
encrypted file server.')

parser.add_argument('operator', type=str, help='What do with the file.',
choices=operators)
parser.add_argument('operand', type=str, help='The file to operate on or 
the search term to look for.')
parser.add_argument('--jobid', type=str, help='The cluster to search 
on.', default=None)

args = parser.parse_args()
\end{lstlisting}

We created a simple text based client that runs on the command line by making use of Python's argparse package.
This takes out most of the work involved with parsing arguments and provides some form of validation of our input.
We pass a list of operators as "choices" when we add the operator argument - argparse will only accept input from
this list. Of course this makes it simple to update the set of commands. \\

It allows us to set jobid as an optional parameter - we set the default to None so that we know to provision a cluster
if the client's command is to search. After we call "args = parser.parse\_args()" we can access our arguments using the
dot operator such as "args.operator".

\begin{figure}[ht]
    \caption{argparse help dialog}
    \centering
    \includegraphics[width=0.9\textwidth]{argparse.png}
\end{figure}

\subsection{Graphical Client}
\begin{figure}[ht]
    \caption{Graphical client}
    \centering
    \includegraphics[width=0.6\textwidth]{guiclient.png}
\end{figure}

The graphical client provides a very simple interface to allow interctions with the application. It makes use of 
our client\_functions module to provide the same functionality as the command line client. We use Tkinter to create
the interface and handle input. The input is passed to the appropriate function in the client\_functions module to 
carry out whichever interaction with S3 or EMR. \\

We make use of message dialogs supplied by Tkinter to relay messages on the progress of our tasks and to inform the 
user of errors. 

\subsection{Cluster Provisioning}
In a real application we would have a constant cluster deployed - in our case it would be unreasonable to do this
for the costs involved and an uncertain timescale for marking and such. We get around this by allowing the user
to pass a cluster ID. \\

When we run a search without supplying a cluster ID a cluster is deployed - if one is supplied we send the job 
to the cluster. As a result of this there is a delay between running a search when a cluster must be deployed - 
this delay is variable in the time it takes for AWS to provision the EC2 instances required.
