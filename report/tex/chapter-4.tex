\chapter{Architecture}

\section{Application Structure}
As we would expect, the application is written using a client-server architecture. We have our distinct 
client applications that interact with the server through a set of predefined interactions. Our server is 
treated as a single unit but is in fact split into separate data storage and logic components. \\

A modular approach has been taken in the structuring of the client side code and client application.
The benefits of this were apparent in development as we will see. \\

Our client applications makes use of the encrypty module through the client\_functions module - this allows
us to abstract any potential common code from each client into a reusable module to make it easier to 
implement further clients.

\begin{figure}[h!]
    \caption{Module relationships}
    \centering
    \includegraphics[width=0.45\textwidth]{module_rel.png}
\end{figure}

\subsection{encrypty.py}
This is our main module containing our encryption operations. It is a small library containing four 
public functions named prep\_search, encrypt\_file, decrypt\_file, search\_file. We also define our boundary
to split encrypted words on for our algorithm and the block size we use - this way it is only defined 
in one place and reused in other modules that might need it. We make use of encapsulation to ensure that 
only relevant functions are public and available for other modules to use. \\

Since most of our encryption functions make use of our hash function, block cipher and stream cipher we move these
into small wrapper functions to remove any duplicate code in instantiating ciphers and encrypting tokens.
While this is mostly motivated by readabiliy we obviously benefit from removing our duplicate code. \\

For the sake of ease of configuration on the AWS side of things we add a section to this module to be run 
when used on AWS - it takes a value from S3 and uses it to search. With this we are able to upload only this
module to S3 to be usable by EMR instead of adding this module and another script to run the code. 

\subsection{client\_functions.py}
This module defines a set of functions to be used by any client application to interact with our service. In
essence, it provides an interface to our encryption library through the server. It contains functions for add,
get, search, delete and valid\_key. \\

The decision to have a valid\_key function in the set of client functions allows us to define a set of criteria for 
the keys used to encrypt. At the moment we only check the size of the key to ensure it is the same as our block size 
but this could easily be extended to provide some sophisticated validation of secure keys. Arguably this should be left
to the client to decide on how secure a key should be but it seemed like a reasonable addition and something that a client 
application might use.

\subsection{client.py}
This is a simple command line client that makes use of the client\_functions module to interact with S3. We can see this 
as the view for our application.(albeit a command line view)

\subsection{gui-client.py}
As with the command line client the GUI client makes use of client\_functions and highlights the benefits of the decision
to move common client functions to a module. We have no duplicate code for our AWS interactions and this allows the client 
applications to handle their inputs and let the client\_functions module handle the work. \\

To go a step further this could be expanded by having client\_functions take string commands and handle those in a similar way
to how the client applications do. This would make it more difficult to give the client application control over how it informs 
the user of events and such.

\section{Amazon Web Services}

\begin{figure}[h!]
    \caption{The architecture used on the server side}
    \centering
    \includegraphics[width=0.8\textwidth]{emr.jpg}
\end{figure}

The application logic and data set is hosted using Amazon Web Services\cite{aws}
and makes use of various products to allow the application to be scalable
in terms of storage space and computing power. \\

\subsection{Simple Storage Service(S3)}

S3\cite{s3} is a key-value store product that allows users to store data in "buckets"
where buckets are automatically scaled based on what is stored in them. Users
pay based on how much storage their buckets use. \\

A point to note is that even though S3 is a key-value store it can be used to store files
as these are essentially large values where one would usually associate key-value stores with
something like JSON or a NOSQL database. \\

We make use of S3 to decouple our file storage from the server logic. In our case
even our code is stored in S3 to be used by EMR - we are completely decoupled from the
machines our code runs on and this allows us a large degree of flexibility for upscaling our
clusters or downscaling as our load dictates. \\

While these benefits are enough for us to use S3 we are required to in order use EMR as it can only
take an input dataset from S3 and our code must be stored in S3.
\subsection{Elastic Compute Cloud(EC2)}

EC2\cite{ec2} allows users access to computing resources in the form of virutal servers.
The user is able to scale the resources of each EC2 instance without much effort
and very quickly to meet peak times and low traffic times. \\

We use EC2 instances as our nodes in the EMR clusters. This adds to the flexibility provided
by S3 as we are able to augment our computational power independently by adding or removing nodes from
our cluster as required.

\subsection{Elastic MapReduce(EMR)}

\begin{lstlisting}[language=Python,frame=lines,basicstyle=\small, numbers=left]
def square(x):
    return x**2

map(square, [2,4,6])
>>>[4,16,36]
\end{lstlisting}

In functional programming we have a map function that calls a function on
some sequence of values and returns a list with the results of these function
calls. This is simple to parallelise since functional programs tend not to have
any state and thus we can avoid race conditions safely. \\

In the example above we see a function defined to take a number and return the square of it.
We map the function to a list of numbers and the output is a list containing the squares of 
the input list. \\

We can extend this with a reduce function that aggregates the data by calculating the total. 
The AWS example for a MapReduce job is a wordcount script that tokenizes a set of files and outputs 
the tokens that are then counted by the reducer. Most commonly it is used to aggregate data but in 
our case we use it to join the chunks created by our multiple mapper jobs\cite{googlemr}.\\

\begin{figure}[h!]
    \caption{MapReduce}
    \centering
    \includegraphics[width=1\textwidth]{mapreduce.png}
\end{figure}

EMR\cite{emr} provides a managed cluster to allow us to parallelise and distribute such tasks without worrying about 
setting up a complicated cluster. We specify a master node that will split up our dataset and pass chunks to our 
worker nodes where the nodes are EC2 instances. It is worth noting that an uneven distribution in the dataset can result 
in worker nodes idling while the other nodes are working. \\

As has been previously mentioned, EMR takes the input data from an S3 bucket - in our case we give it the bucket where
we store our encrypted documents. We also point it to a mapper and reducer on S3 - text scripts and binaries are supported 
although binaries must be compiled using Amazon Linux(the distro used by EMR nodes). Our master node delegates a set of files to 
each worker nodes and they run the mapper on each of these and pass the output to the reducer. It is possible to delegate a 
reducer node although this is not essential. \\

To put it simply, EMR is what binds the S3 and EC2 services together for us. It manages a group of EC2 instances as a cluster and
takes its input and output from S3. This makes it easy for us to give the impression of a complete file server when in fact the pieces 
are decoupled. 
