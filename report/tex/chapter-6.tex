\chapter{Verification and Validation}
This chapter will discuss the testing methods used to validate the application. We will also look at the tools
that were available and how they were able to help us.

\section{Test Data}
We tested the application using a set of transcripts from a few lecture series\cite{warwick} in plain text format. These were 
obtained using a Python script wrapping around wget.

\section{Debugging}
While there are a variety of debuggers available for Python we mostly made use of print statements to show what
was happening in the code if a result was different than expected. Due to the encrypty module being comprised of small
functions this proved to be effective as each function has a defined purpose and makes it easy to know where a bug may be. \\

Moving to AWS made debugging much more difficult as STDOUT is used as the return output and printing would mess with the output.
Instead we write to STDERR. Every node in the cluster has their own set of outputs including STDOUT, STDERR and SYSLOG - this makes
finding the correct log rather difficult. Each node has their own folder with a respective STDOUT, STDERR and SYSLOG files and if 
one node fails the others are cancelled and the cluster is shut down. This leads to some time spent finding which node had the error.

\section{Tools}
\subsection{Vimdiff}
We made use of vimdiff to compare decrypted documents against the original plaintext they were encrypted from.
This provided a simple form of verification that we were indeed encrypting and subsequently decrypting the files
correctly. 

\begin{figure}[h!]
    \caption{Vimdiff showing differences}
    \centering
    \includegraphics[width=0.9\textwidth]{vimdiff.png}
\end{figure}

The difference highlighted in the figure shows a difference between the original plaintext and the result of encrypting and decrypting this text.
The difference between the two is that the original has a token over 16 characters long and is truncated to our block size of 16. This has been massively
helpful in checking that we are correctly encrypting and decrypting.

\subsection{pytest}
We use pytest for our unit testing - the Python standard library has a unittest module that provides similar functionality to pytest. We don't make use of 
any advanced pytest features and use it mostly out of personal preference.

\section{Verification}

\subsection{Unit Tests}
Unit tests have been used rather sparingly to cover some simple test cases for our encrypty module. This is for a few reasons, namely 
that it is rather difficult to properly test the application as it makes use of a random initialisation vector. This small thing completely
changes our output and means we are unable to manually calculate a small example as the initialisation vector will be different. Another reason
is due to the fact that we write to file. This could be improved by returning a string containing the ciphertext/plaintext and let the client do
what they will with it be that writing to a file or saving in a database. \\

This would allow us to test with something similar to the following: \\
\begin{lstlisting}[frame=lines,basicstyle=\small, numbers=left]
def test_encrypt(s):
    assert decrypt(encrypt(s)) == s
\end{lstlisting}
~\\
One of the current test cases works in a similar manner by encrypting a file, decrypting and then checking the contents of the file. Of course the suggested
method is much more elegant.\\

The application on AWS is rather difficult to test through any means other than manual testing as our files are stored remotely and so is the logic behind our search.\\

Since our encrypty module is to be used as a small library we make use of encapsulation to keep most of the functions private and only give access to the functions 
that a client application would need. As a result our unit tests only cover these public functions. \\

While it might seem strange at first we don't test our encrypty module using any unexpected inputs. In Python readability is an important factor and to keep our functions
focused on the problem they solve we leave any error checking to client applications that use them. With Python being loosely typed we can run our functions
with just about any input available and it will throw type errors and such. When we think more on this it makes sense that we don't check these things since at that 
point we may as well use a strictly typed language. \\

Of course this means we must sanitize any input on the client side before we pass it to our encryption functions and the server or at the very least catch the exceptions
that these function calls raise. We let the client applications know what input we allow by documenting it in our comment docstrings. For example our prep\_search docstring 
looks like this:

\begin{lstlisting}[frame=lines,basicstyle=\small, numbers=left]
""" Expands the term passed as a regular expression and prepares each 
    of the expansions as a search term, 
    returning a list of tuples with the pre-encrypted search term and 
    word specific key.

    Keyword Arguments:
    term -- The term to be searched for. Should be 16 characters or 
    less and not have any whitespace.
    hash_key -- The key to be used by our hash function to generate a 
    word specific key. 
    block_key -- The key to be used by our block cipher. Must be 
    16 characters in length.
"""
\end{lstlisting}
\subsection{Bug Tracking}
As was previously discussed we used Bitbucket for version control - we also made use of the issue tracker it provides. It allows us to keep a track of known issues with the
system and to address them as they are fixed. In terms of working on this in the future it provides a log of the issues we have found and allows us to delegate tasks to 
anybody that is working on the project.

\section{Black Box Testing}
As has been explained, we leave error checking to the client. We adopt a manual black box testing approach to cover the cases and to ensure that our application behaves
as we would expect even when given erroneous input. \\

Since we don't have very complex user interfaces our manual approach isn't unreasonable. In both the text client and graphical client we try a combination of valid and invalid 
inputs while checking that the application behaves in an appropriate manner such as displaying an error message if a key is not 16 characters long. \\

As a result of this testing we uncovered a rather large bug where the graphical client would accumulate search terms without clearing the list each time. For example if we searched
"cold" and our prep\_search function returned a list with one tuple and ran it again we would end up with two tuples instead of the expected one tuple. Since the text based client
works one transaction per run this went unnoticed for a while into development. Since we could easily trace the error to the prep\_search and expand\_regex functions we discovered
that the problem was caused by references being used instead of values. \\

In a larger application this manual approach would not be feasible but since we have a small amount of user interactions to cover it suffices for our purposes.  \\

\begin{figure}[ht]
    \caption{Correct input gives expected output}
    \centering
    \includegraphics[width=0.9\textwidth]{correctoutput.png}
\end{figure}

As an example, one of our test cases involves completely correct input. If the user inputs valid keys that have been used to encrypt some files on our server and searches using a valid 
search term we expect a message containing the results of the search.


\section{AWS Testing}
Testing the application on AWS in any automated manner proved difficult. Since the input is passed via STDIN we cannot programmatically write test cases like with our other functions and 
testing locally only proves that our logic is sound since the EC2 instances run a different operating system with different defaults. \\

\begin{figure}[ht]
    \caption{EMR Debugging Mode}
    \centering
    \includegraphics[width=0.9\textwidth]{emr_debugging.png}
\end{figure}

We made extensive use of the AWS developer console for this and enable debugging mode for EMR to give us some insight into how our code is executed. Debugging mode allows us to see which nodes
are being assigned which "mapper task" and find out if any have failed. For the most part this was a frustrating process as the cluster shuts down if there is an error instead of waiting for another
job. This meant that each time the code was run and an error occurred we would wait up to ten minutes for another cluster to be provisioned.(even with only one node for testing purposes) \\

Fortunately, once we had the code working from the developer console we were easily able to run the application from our local script using boto. Nevertheless, there definitely could have
been improvements in testing the application on AWS although none are immediately obvious.
\section{Bugs Found}
Most of the issues discovered as a result of our testing were related to invalid inputs - we solve these issues by including small functions to validate our inputs before doing anything
with them. Of course not all valid inputs are correct and there are issues with files not existing on S3 or cluster IDs not existing. We solve these by making use of exception handling - 
as previously mentioned our logic code doesn't check for errors and lets the client deal with it. \\

\begin{lstlisting}[language=Python,frame=lines,basicstyle=\small, numbers=left]
try:
    get(filename, block_key, stream_key, hash_key)
except S3ResponseError as e:
    sys.stderr.write(e.message)
    return
\end{lstlisting}
~\\
Our solution to this is to make use of exception handling to deal with any incorrect input. In the above example we run our get method that downloads a file from S3 and decrypts it within a try
block. If the get method raises an S3ResponseError we write out the message to STDERR and return, ending execution. This allows us to gracefully handle errors instead of flooding the screen with 
stack traces.

\subsection{Non-Text Files}
As a result of our testing we discovered that our scheme does not work on other document types such as images. Any non-text file that is encrypted using our application is damaged and presumed unrepairable.
This is likely due to us truncating tokens over 16 bytes and removing information in the process. While it is of little importance that our application works on such files it could have been a convenient 
feature to add support for encrypting these files but not search. A slight improvement to our application would have been to disallow files with extensions other than a few known text file extensions.

\section{Summary}
Testing has been a large part of the project and while most of it couldn't be automated and took a more informal approach we definitely reaped the benefits and have a more robust application as a result.
If at all possible it would have been nice to be able to test our AWS functions in an automated way but at the very least we reached our goal of a working application.
